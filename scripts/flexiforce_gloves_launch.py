#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import roslaunch
import rosparam
import sys

def flexiforce_gloves_launch():
    rospy.init_node('flexiforce_glove_launch', anonymous=True)

    index = (sys.argv[0]).find('flexiforce_glove')
    root_path = (sys.argv[0])[0:index]

    flexiforce_gloves = rosparam.load_file(root_path + 'flexiforce_glove/config/flexiforce_gloves.yaml')
    rospy.set_param('flexiforce_gloves_temp', (flexiforce_gloves[0])[0])
        
    launch = roslaunch.scriptapi.ROSLaunch()
    launch.start()

    process = []

    for element in (flexiforce_gloves[0])[0]:
        flexiforce_glove = rosparam.load_file(root_path + 'flexiforce_glove/config/' + element + '.yaml')

        package = 'flexiforce_glove'
        executable = 'flexiforce_joint_state_publisher'

        rospy.set_param('flexiforce_gloves/' + element + "/", (flexiforce_glove[0])[0])
        rospy.set_param('flexiforce_gloves/' + element + "/hand_name", element)
        flexiforce_joint_state_publisher = roslaunch.core.Node(package, executable, element, 'flexiforce_gloves')

        process.append(launch.launch(flexiforce_joint_state_publisher))

        rospy.set_param('flexiforce_gloves/' + element + '/arduinoBoard/isConnected', rospy.get_param('flexiforce_gloves_temp/' + element + '/isArduinoBoardConnected'))

        if rospy.get_param('flexiforce_gloves/' + element + '/arduinoBoard/isConnected'):
            package = 'rosserial_python'
            executable = 'serial_node.py'

            rospy.set_param('flexiforce_gloves/' + element + '/arduinoBoard/port', rospy.get_param('flexiforce_gloves_temp/' + element + '/arduinoBoard_port'))
            rospy.set_param('flexiforce_gloves/' + element + '/arduinoBoard/baud', rospy.get_param('flexiforce_gloves_temp/' + element + '/arduinoBoard_baud'))
            flexiforce_arduino = roslaunch.core.Node(package, executable, 'arduinoBoard', 'flexiforce_gloves/' + element);
            process.append(launch.launch(flexiforce_arduino))

        rospy.delete_param('flexiforce_gloves_temp/' + element)

    rate = rospy.Rate(0.2) # 0.2 Hz --> 5s

    while not rospy.is_shutdown():
        rate.sleep()

        for index in range(len(process)):
            if not process[index].is_alive():
                return

if __name__ == '__main__':
    try:
        flexiforce_gloves_launch()
    except rospy.ROSInterruptException:
        pass