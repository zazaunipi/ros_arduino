#include <Servo.h>

Servo servolefta; //define left servo, leg a
Servo servorighta; //define right servo, leg a

Servo servoleftb; //define left servo, leg b
Servo servorightb; //define right servo, leg b

Servo servoleftc; //define left servo, leg c
Servo servorightc; //define right servo, leg c

int value1a,value2a,value1b,value2b,value1c,value2c,input,incr, decr;
int pos_ref1a,pos_ref1b,pos_ref1c; 

char bytesread[20];
char byte_r[6];
int comma_point[6];
int j,i,gamma;
int n = sizeof(bytesread);
int initial_value = 90;

void setup() {
  
  servolefta.attach(3); //set left servo a to digital pin 3
  servorighta.attach(9); //set right servo a to digital pin 9
  servoleftb.attach(10); //set left servo b to digital pin 10
  servorightb.attach(11); //set right servo b to digital pin 11
  servoleftc.attach(5); //set left servo c to digital pin 6
  servorightc.attach(6); //set right servo c to digital pin 5
  
  
  value1a = initial_value; //value1a is the value 'zero' of the servo 1a
  value2a = initial_value;
  value1b = value1a;
  value2b = value2a;
  value1c = value1a;
  value2c = value2a;
  incr = 1;
  decr = 1;

  //servolefta.write(value2a);
  //servorighta.write(value1a);
  //servoleftb.write(value2b);
  //servorightb.write(value1b);
  //servoleftc.write(value2c);
  //servorightc.write(value1c);
  
  Serial.begin(115200);
}

void loop() {
  if (Serial.available()) {
      Serial.readBytesUntil('\n', bytesread, sizeof(bytesread));
      
      Serial.println(bytesread);
      j = 0;
      for (i=0; i < n; i++) {
        if (bytesread[i] == 44) {comma_point[j] = i; j = j +1;}
      }
      gamma = comma_point[1] - comma_point[0];
      comma_point[0] = comma_point[0] + 1;
      j = 0;
      for (i = comma_point[0]; i < comma_point[1]; i++) {
        byte_r[j] = bytesread[i];
        j = (j + 1);
      }
      pos_ref1a = atoi(byte_r);
      
      comma_point[0] = comma_point[0] - 1;
      for (i = 0; i<6; i++ ){
      byte_r[i] = 0;
      } 
      
      gamma = comma_point[2] - comma_point[1];
      comma_point[1] = comma_point[1] + 1;
      j = 0;
      for (i = comma_point[1]; i < comma_point[2]; i++) {
        byte_r[j] = bytesread[i];
        j = (j + 1);
      }
      pos_ref1b = atoi(byte_r);
      comma_point[1] = comma_point[1] - 1;
      for (i = 0; i<6; i++ ){
      byte_r[i] = 0;
      } 
      
      gamma = comma_point[3] - comma_point[2];
      comma_point[2] = comma_point[2] + 1;
      j = 0;
      for (i = comma_point[2]; i < comma_point[3]; i++) {
        byte_r[j] = bytesread[i];
        j = (j + 1);
      }
      pos_ref1c = atoi(byte_r);
      comma_point[2] = comma_point[2] - 1;

  //    Serial.println(value1b, DEC);
    //  Serial.println(value1c, DEC);
    //input = Serial.read();
    /*if (input == 43) { //se ingresso è + (codifica ASCII 43 è tasto +)
    value1a = value1a + incr;
    value2a = value2a - incr;
      if (value1a > 180) value1a = 180;
      if (value2a < 0) value2a = 0;
    value1b = value1a;
    value2b = value2a;
    value1c = value1a;
    value2c = value2a;  
    */
    
    value2a = value2a-(pos_ref1a - value1a);
    value1a = pos_ref1a;
    value2b = value2b-(pos_ref1b - value1b);
    value1b = pos_ref1b;
    value2c = value2c-(pos_ref1c - value1c);
    value1c = pos_ref1c;    

    servolefta.write(value2a);
    servorighta.write(value1a);
    servoleftb.write(value2b);
    servorightb.write(value1b);
    servoleftc.write(value2c);
    servorightc.write(value1c); 
    
    /*}
  
    if (input == 45) { //se ingresso è - (codifica ASCII 45 è tasto -)
    value1a = value1a - decr;
    value2a = value2a + decr;
      if (value1a < 0) value1a = 0;
      if (value2a > 180) value2a = 180;
    value1b = value1a;
    value2b = value2a;
    value1c = value1a;
    value2c = value2a;  
    servolefta.write(value2a);
    servorighta.write(value1a);
    servoleftb.write(value2b);
    servorightb.write(value1b);
    servoleftc.write(value2c);
    servorightc.write(value1c);  
    }
       
   Serial.print("value1a= ");
   Serial.print(value1a, DEC);
   Serial.print(" , value2a= ");
   Serial.println(value2a, DEC);
    
    if (input == 49) { //se ingresso è 1 (codifica ASCII 49 è tasto 1)
    value1a = value1a + incr;
    value2a =  value2a - incr;
      if (value1a > 180) value1a = 180;
      if (value2a < 0) value2a = 0;
    servorighta.write(value1a);
    servolefta.write(value2a);
    }
    
    if (input == 50) { //se ingresso è 2 (codifica ASCII 50 è tasto 2)
    value1a = value1a - decr;
    value2a = value2a + decr;
      if (value1a < 0) value1a = 0;
      if (value2a > 180) value2a = 180;
    servolefta.write(value2a);
    servorighta.write(value1a);
    }
    
    if (input == 51) { 
    value1b = value1b + incr;
    value2b =  value2b - incr;
      if (value1b > 180) value1b = 180;
      if (value2b < 0) value2b = 0;
    servorightb.write(value1b);
    servoleftb.write(value2b);
    }
    
    if (input == 52) {
    value1b = value1b - decr;
    value2b = value2b + decr;
      if (value1b < 0) value1b = 0;
      if (value2b > 180) value2b = 180;
    servoleftb.write(value2b);
    servorightb.write(value1b);
    }
    
    if (input == 53) { 
    value1c = value1c + incr;
    value2c =  value2c - incr;
      if (value1c > 180) value1c = 180;
      if (value2c < 0) value2c = 0;
    servorightc.write(value1c);
    servoleftc.write(value2c);
    }
    
    if (input == 54) {
    value1c = value1c - decr;
    value2c = value2c + decr;
      if (value1c < 0) value1c = 0;
      if (value2c > 180) value2c = 180;
    servoleftc.write(value2c);
    servorightc.write(value1c);
    }
    */
    }
}
    
  
  
  
