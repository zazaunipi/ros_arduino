#include <string>
#include <ros/console.h>
#include <ros/ros.h>
#include <iostream>
#include <stdio.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <termios.h>
// #include <string.h> // needed for memset
// #include <stdint.h>
// #include <time.h>
// #include <eigen3/Eigen/Eigen>
// #include <unistd.h>
// #include <fstream>
#include <serial/serial.h>
// #include <math.h>
// #include <ctime>
// #include <cstdlib>
// #include <cstring>
// #include <complex>
// #include <signal.h>
// #include <eigen3/Eigen/SVD>
// #include <sys/time.h>
#include <sensor_msgs/JointState.h>
// #include <stddef.h>
// #include <stdint.h>

// using namespace Eigen;
using namespace std;

static uint8_t s_cExtrReachMask;

//GLOBAL VARIABLES
float pi = 3.1415;
int tty_fd;
char *porta =(char*)"/dev/ttyACM0";
bool init=false;
serial::Serial serial_port;
ros::Subscriber sub;

//Arduino FUNCTION
void init_Arduino(){
        // struct termios tio;
        // struct termios stdio;
        // fd_set rdset;

        serial_port.setPort("/dev/ttyACM0");
        serial_port.setBaudrate(115200);
	serial::Timeout a = serial::Timeout::simpleTimeout(1000);
	serial_port.setTimeout(a);
        serial_port.open();
	if (!serial_port.isOpen())
		abort();
        // memset(&stdio,0,sizeof(stdio));
        // stdio.c_iflag=0;
        // stdio.c_oflag=0;
        // stdio.c_cflag=0;
        // stdio.c_lflag=0;
        // stdio.c_cc[VMIN]=1;
        // stdio.c_cc[VTIME]=0;
        // tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
        // tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
        // fcntl(STDIN_FILENO, F_SETFL);       // make the reads non-blocking
        //
        // memset(&tio,0,sizeof(tio));
        // tio.c_iflag=0;
        // tio.c_oflag=0;
        // tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
        // tio.c_lflag=0;
        // tio.c_cc[VMIN]=1;
        // tio.c_cc[VTIME]=0;
        //
        // tty_fd=open(porta, O_RDWR );
        // int out_s= cfsetospeed(&tio,B115200);
        // int in_s= cfsetispeed(&tio,B115200);
        //
        // tcsetattr(tty_fd,TCSANOW,&tio);
}

int write_Servo(int a, int b, int c){
if (!init) return 0;
        char d[30];
        memset(d, 0, sizeof(d));
        char ca[30], cb[30], cc[30];
        sprintf(ca, "%d", a);
        sprintf(cb, "%d", b);
        sprintf(cc, "%d", c);
        strcpy(d, ",");
        strcat(d, ca);
        strcat(d, ",");
        strcat(d, cb);
        strcat(d, ",");
        strcat(d, cc);
        strcat(d,",\n");
	//std::string t(",60,60,60,\n");
std::string t(d);
    //size_t bytes_wrote =       serial_port.write((uint8_t*)d,strlen(d));
        cout << t << endl;
    size_t bytes_wrote = serial_port.write(t);

    string result = serial_port.read(t.length()+1);

    cout << bytes_wrote << ", Bytes read: ";
    cout << result.length() << ", String read: " << result << endl;
    // std::string msg("," + std::to_string(a) + "," + std::to_string(b) + "," + std::to_string(c) + ",");
    // ROS_INFO_STREAM(msg);
    // serial_port.write(msg);
}

void cb_jnt_cmd (const sensor_msgs::JointState::ConstPtr &msg)
{
    ROS_INFO("callb");
    double a,b,c;
    for (size_t i=0; i< msg->name.size(); ++i)
    {
        if (msg->name[i].compare("A_first_joint") == 0)
            a = msg->position[i];
        else if (msg->name[i].compare("B_first_joint") == 0)
            b = msg->position[i];
        else if (msg->name[i].compare("C_first_joint") == 0)
            c = msg->position[i];
        else
            continue;
    }
    write_Servo(a*180.0/M_PI,b*180.0/M_PI,c*180.0/M_PI);
}

// A_first_joint
// B_first_joint
// C_first_joint
/////////////////////////////////MAIN////////////////////////////////////////////////////
int main(int argc,char** argv)
{
    ros::init(argc,argv, "lemon_delta_firmaware");
    ros::NodeHandle nh;
    //local variables definition
    sub = nh.subscribe<sensor_msgs::JointState>("/joint_states",1, &cb_jnt_cmd);
    init_Arduino();
sleep(5);
init=true;    

    ros::Rate rate(10);
    // write_Servo(90,90,90);
    while(nh.ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}// end main
